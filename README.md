# EgisWpfLibrary

Cette librairie met à votre disposition des styles et des contrôles pour vos applications WPF. Elle est basée sur le framework Telerik for WPF ainsi que sur le Design System Egis. Les contrôles de base de WPF ainsi que les contrôles personnalisés sont stylisés pour correspondre au design system Egis.

## Usage

### Installation

Pour utiliser cette librairie, suivez les étapes suivantes :

1. **Créer un [personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html)**

   - Suivez les instructions fournies dans le lien pour générer un token GitLab.

2. **Enregistrer le token dans les variables d'environnement**

   - Ajoutez votre token comme variable d'environnement pour permettre l'authentification auprès du registre privé GitLab.

3. **Enregistrer le private registry**

   - Exécutez la commande suivante dans la Console du Gestionnaire de Package de Visual Studio :
     ```powershell
     Register-PSRepository -Name "NomDuRegistre" -SourceLocation "URLDuRegistre" -InstallationPolicy Trusted -Credential (Get-Credential)
     ```

4. **Installer le package Nuget**

   - Exécutez la commande suivante dans la Console du Gestionnaire de Package :
     ```powershell
     Install-Package EgisWpfLibrary
     ```

5. **Ajouter la ressource dans App.xaml**

   - Ajoutez le code suivant dans le fichier App.xaml de votre projet :
     ```xaml
     <ResourceDictionary>
       <ResourceDictionary.MergedDictionaries>
            <ResourceDictionary Source="pack://application:,,,/EgisWpfLibrary;component/Controls/EgisControls.xaml"/>
            <ResourceDictionary Source="pack://application:,,,/EgisWpfLibrary;component/Themes/EgisTheme.xaml"/>
       </ResourceDictionary.MergedDictionaries>
     </ResourceDictionary>
     ```

6. **Utiliser les styles et les contrôles dans votre application**

   - Vous pouvez maintenant utiliser les styles et les contrôles fournis par la librairie dans votre application WPF.

### Contrôles

Voici la liste des contrôles disponibles :

_Pas encore disponible_

## Contribution

### Processus

Si vous souhaitez contribuer à cette librairie, suivez les étapes suivantes :

1. **Cloner le [repository](https://gitdoit.egis.fr/deb-digital-factory/difa-labs/difa-wpf-xaml)**

   - Utilisez la commande Git suivante pour cloner le repository sur votre machine :
     ```bash
     git clone https://gitdoit.egis.fr/deb-digital-factory/difa-labs/difa-wpf-xaml
     ```

2. **Ouvrir le projet dans Visual Studio**

   - Utilisez Visual Studio pour ouvrir le projet cloné.

3. **Modifier le code**

   - Effectuez les modifications nécessaires dans le code, en vous basant sur les directives du design system Egis.

4. **Commit, push et créer une merge request**

   - Utilisez Git pour effectuer un commit de vos modifications, poussez-les vers le repository, et créez une merge request.

5. **Faire valider code par pairs et fonctionnalité par PO**

   - Assurez-vous que vos modifications sont examinées par vos pairs et validées par le Product Owner.

6. **Merge request validée, le package est automatiquement publié sur le registre privé**

   - Une fois que la merge request est validée, le processus automatisé publie le package sur le registre privé GitLab.

### Considérations

- **Se baser sur le Figma du design system Egis pour créer les styles et les contrôles**

  - Utilisez le Figma du design system Egis comme référence principale pour garantir la cohérence visuelle.

- **Chaque contrôles a son propre fichier style dans le dossier Styles**

  - Assurez-vous que les fichiers de style pour chaque contrôle sont bien organisés dans le dossier Styles.

- **Les styles sont regroupés dans le fichier Main.xaml**

  - Tous les styles sont regroupés dans le fichier Main.xaml pour une gestion plus efficace.

- **Tester les contrôles dans MainWindow.xaml**

  - Effectuez des tests approfondis des contrôles dans MainWindow.xaml pour garantir leur bon fonctionnement.
